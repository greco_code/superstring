# Superstring generator

## Описание алгоритма
1. Получаем массив строк
2. Перебором выясняем, у каких двух строк больше пересечений друг с другом
3. Склеиваем эти строки удаляя пересечение с одной стороны
4. Обновляем массив
5. Проделываем то же самое до тех пор, пока не останется один элемент в массиве. Это и будет реузльтатом.

Алгоритм лежит в utils -> utils.ts

## Deploy
Deployed with Vercel<br>
Deploy link -- https://superstring.vercel.app


## Scripts

### Install
```bash
yarn install 
# or
npm install
```

### Run dev version (localhost:3000 by default)
```bash
npm run dev
# or
yarn dev
```

### Run tests
```bash
jest
```

