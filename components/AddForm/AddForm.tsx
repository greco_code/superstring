import React, {DetailedHTMLProps, Dispatch, HTMLAttributes, SetStateAction, useState} from 'react';
import {FormControl, Input, InputLabel} from "@mui/material";
import Button from "@mui/material/Button";
import {ONLY_LETTERS} from "@/utils/const";
import {IChipData} from "@/components/Chips/interface";
import {IInputState} from "@/components/AddForm/interface";
import {InputCaption} from "@/components/InputCaption/InputCaption";

interface IProps extends DetailedHTMLProps<HTMLAttributes<HTMLFormElement>, HTMLFormElement> {
    chipData: IChipData[];
    setChipData: Dispatch<SetStateAction<IChipData[]>>;
    input: IInputState<string>;
    setInput: Dispatch<SetStateAction<IInputState<string>>>;
    className?: string;
}

export const AddForm = ({className, chipData, setChipData, setInput, input, ...props}: IProps): JSX.Element => {
    const [buttonDisabled, setButtonDisabled] = useState(true);

    const onAddSubmit = (evt: React.FormEvent<HTMLFormElement>): void => {
        evt.preventDefault();
        setChipData((prev) => [...prev, {key: chipData.length, label: input.value}]);
        setInput({
            value: '',
            error: '',
        });
    }

    const onInput = (word: string): (void | null) => {
        if (validateWord(word)) {
            setInput({
                value: word,
                error: `Can contain only letters`,
            });
            setButtonDisabled(true);
            return null;
        }

        setButtonDisabled(false)
        setInput({value: word, error: ''});
    }

    const validateWord = (word: string): boolean => {
        // Спецом оставил иф, так как в тоерии может быть больше условий и тернарник все равно придется убрать
        if (!ONLY_LETTERS.test(word)) {
            return false;
        }

        return true;
    };

    return (
        <form
            onSubmit={(evt) => {
                onAddSubmit(evt)
            }}
            style={{
                display: 'flex',
                alignItems: 'center',
                gap: '30px',
                marginBottom: '10px',
            }}
            className={className}
            {...props}
        >
            <FormControl
                variant={'standard'}
                margin={'dense'}
                fullWidth={true}
            >
                <InputLabel htmlFor="give-admin">
                    Your word
                </InputLabel>
                <Input
                    id="give-admin"
                    aria-describedby="my-helper-text"
                    required={true}
                    value={input.value}
                    onChange={(evt) => {
                        onInput(evt.currentTarget.value.trim())
                    }}
                />
                <InputCaption defaultLabel={'Add a word to the list to generate a superstring'} error={input.error}/>
            </FormControl>
            <Button
                variant={'contained'}
                type={'submit'}
                disabled={buttonDisabled || !input.value}
            >
                Add
            </Button>
        </form>
    );
};
