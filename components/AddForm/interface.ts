export interface IInputState<T> {
    value: T;
    error?: string;
}