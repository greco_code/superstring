import React, {Dispatch, SetStateAction} from 'react';
import {Chip, Paper} from "@mui/material";
import Typography from "@mui/material/Typography";
import {IChipData} from "@/components/Chips/interface";

interface IProps {
    chipData: IChipData[];
    setChipData: Dispatch<SetStateAction<IChipData[]>>;
    className?: string;
}

export const Chips = ({ chipData, setChipData, className }: IProps): JSX.Element => {
    const handleDelete = (chipToDelete: IChipData) => (): void => {
        setChipData((chips) => chips.filter((chip) => chip.key !== chipToDelete.key));
    };

    return (
        <Paper
            sx={{
                display: 'flex',
                justifyContent: 'left',
                alignItems: 'center',
                flexWrap: 'wrap',
                listStyle: 'none',
                p: 1,
                m: 0,
                mb: 3,
                boxShadow: 'none',
            }}
            component="ul"
            className={className}
        >
            {chipData.length
                ? chipData.map((data) => (
                    <Chip
                        key={data.key}
                        label={data.label}
                        onDelete={data.label === 'React' ? undefined : handleDelete(data)}
                        sx={{margin: '3px'}}
                    />
                ))
                : <Typography component={'span'} sx={{color: 'gray', fontSize: '12px'}}>Empty</Typography>
            }
        </Paper>
    );
};
