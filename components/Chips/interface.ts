export interface IChipData {
    key: number;
    label: string;
}