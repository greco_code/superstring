import React from 'react';
import {FormHelperText} from "@mui/material";

interface IProps {
    defaultLabel: string;
    error: string;
    className?: string;
}

export const InputCaption = ({defaultLabel, error, className}: IProps): JSX.Element => {

    return (
        <FormHelperText id="my-helper-error" sx={error ? {color: 'red'}: {}} className={className}>
            {error || defaultLabel}
        </FormHelperText>
    );
};
