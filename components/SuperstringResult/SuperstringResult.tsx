import React from 'react';
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

interface IProps {
    superstring: string | null;
    className?: string;
}

export const SuperstringResult = ({ superstring, className }: IProps): JSX.Element => {

    return (
        <Box className={className}>
            <Typography variant={'h6'}>Your superstring:</Typography>
            <Typography color={'gray'}>{superstring || 'Add words to the list first'}</Typography>
        </Box>
    );
};
