import React, {ReactNode} from 'react';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import Typography from "@mui/material/Typography";
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';

interface Props {
    heading?: string;
    children: ReactNode;
}

export const Layout = ({heading, children}: Props): JSX.Element => {
    const theme = createTheme();

    return (
        <ThemeProvider theme={theme}>
            <Container maxWidth={'sm'} sx={{padding: '50px 0'}}>
                {heading && <Typography align={'center'} component={'h1'} variant={'h4'} mb={4}>{heading}</Typography>}
                <Box
                    sx={{
                        bgcolor: '#e0e0e0',
                        padding: '30px 30px 50px 30px',
                        borderRadius: '10px',
                    }}
                >
                    {children}
                </Box>
            </Container>
        </ThemeProvider>
    );
};
