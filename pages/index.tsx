import * as React from 'react';
import {NextPage} from "next";
import {useEffect, useState} from "react";
import {getSuperstring} from "@/utils/utils";
import {AddForm} from "@/components/AddForm/AddForm";
import {Chips} from "@/components/Chips/Chips";
import {SuperstringResult} from "@/components/SuperstringResult/SuperstringResult";
import {Layout} from "@/layout/Layout";
import {IChipData} from "@/components/Chips/interface";
import {IInputState} from "@/components/AddForm/interface";

const Home: NextPage = () => {
    const [chipData, setChipData] = useState<IChipData[]>([]);
    const [input, setInput] = useState<IInputState<string>>({
        value: '',
        error: '',
    });
    const [superstring, setSuperstring] = useState<string | null>(null);

    useEffect(() => {
        const words = chipData.map((word) => word.label);
        setSuperstring(getSuperstring(words));
    }, [chipData])

    return (
        <Layout heading={'Superstring generator'}>
            <AddForm chipData={chipData} setChipData={setChipData} input={input} setInput={setInput}/>
            <Chips chipData={chipData} setChipData={setChipData}/>
            <SuperstringResult superstring={superstring}/>
        </Layout>
    );
}

export default Home;