import {getSuperstring} from "../utils/utils";
// @ts-ignore
import {toBeOneOf} from 'jest-extended';

// Если ожидается одно значение, то используется toBe(string)
// Если ожидается несколько значений(строки могут быть в любом порядке), то toBeOneOf(string[])

describe('Home', () => {
    it('generates the shortest superstring', () => {
        expect(getSuperstring(['ab', 'bc', 'cd', ])).toBe('abcd');
        expect(getSuperstring(['ab', 'sadf', 'v', ])).toBeOneOf(['absadfv', 'asvsadf', 'sadfabv', 'sadfbav', 'vabsadf']);
        expect(getSuperstring(['a b,', ' b,', 'b', ])).toBe('a b,');
        // Английские и русские буквы
        expect(getSuperstring(['рp', 'pр'])).toBe('рpр');
        expect(getSuperstring(['123', '234'])).toBe('1234');
        expect(getSuperstring(['1234567890', '456', '456', '456', '234', 'foo', 'foo', 'bar', 'foobar']))
            .toBeOneOf(['1234567890foobar', 'foobar1234567890']);
    })
})