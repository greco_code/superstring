// { [second word index in array]: [number of overlaps between two words, new word] }
type IWordToOverlap = { [key: string]: [number, string] };
// { [first word index in array]: IWordToOverlap }
type IAllOverlapIndexes = { [key: string]: IWordToOverlap };
// index of the first word in pair which has most overlaps,
// index of the second,
// new word,
// the longest overlap length
type IWordsWithMostOverlaps = {
    firstWordIndex: number,
    secondWordIndex: number,
    word: string;
    mostOverlapsLength: number,
}

// const getWordPairOverlaps = (firstWord: string, secondWord: string, copySecondWord: string, initialSecondWord?: string): { overlap: string, newWord: string } => {
//
//     if (secondWord.length === 0 && !!initialSecondWord) {
//         return getWordPairOverlaps(initialSecondWord, firstWord, firstWord);
//     }
//
//     if (secondWord.length === 0 && !initialSecondWord) {
//         return {
//             overlap: '',
//             newWord: firstWord + copySecondWord,
//         };
//     }
//
//     if (firstWord.endsWith(secondWord)) {
//         return {
//             overlap: secondWord,
//             newWord: firstWord.slice(0, firstWord.length - secondWord.length) + copySecondWord,
//         };
//     }
//
//     return getWordPairOverlaps(firstWord, secondWord.substring(0, secondWord.length - 1), copySecondWord, initialSecondWord);
// }

const getWordPairOverlaps = (a, b, originalB, reverse) => {
    if (!originalB) {
        originalB = b
    }

    if (b.length === 0 && !reverse) {
        return getWordPairOverlaps(originalB, a, a, true)
    }

    if (a.endsWith(b)) {
        return {
            overlap: b,
            newWord: a + originalB.slice(b.length, originalB.length),
        }
    }
    if (a.indexOf(b) >= 0 && b.length === originalB.length) {
        return  {
            overlap: b,
            newWord: a,
        }
    }

    if (!reverse) {
        return getWordPairOverlaps(a, b.substring(0, b.length - 1), originalB, false)
    } else {
        return getWordPairOverlaps(a, b.substring(0, b.length - 1), originalB, true)
    }
}

const getAllOverlapsIndexes = (words: string[]): IAllOverlapIndexes => {
    let wordsPairsOverlapIndexes: IAllOverlapIndexes = {}

    words.forEach((firstWord, firstWordIndex) => {
        let wordToOverlapLength: IWordToOverlap = {};
        wordsPairsOverlapIndexes[firstWordIndex] = {};

        words
            .forEach((secondWord, secondWordIndex) => {
                if (secondWord === firstWord) {
                    // for not iterating through the same word
                    return;
                }

                const wordPairOverlaps = getWordPairOverlaps(firstWord, secondWord, secondWord, secondWord);

                wordToOverlapLength[secondWordIndex] = [wordPairOverlaps.overlap.length, wordPairOverlaps.newWord];

                wordsPairsOverlapIndexes[firstWordIndex] = wordToOverlapLength;
            })
    })

    return wordsPairsOverlapIndexes;
}

const getWordsWithMostOverlaps = (allOverlapsIndexes: IAllOverlapIndexes): IWordsWithMostOverlaps => {
    let firstWordIndex = 0;
    let secondWordIndex = 1;
    let mostOverlapsLengthCounter = 0;
    let word = allOverlapsIndexes[0][1][1];

    for (const [firstKey, firstValue] of Object.entries(allOverlapsIndexes)) {
        for (const [secondKey, secondValue] of Object.entries(allOverlapsIndexes[firstKey])) {
            if (secondValue[0] > mostOverlapsLengthCounter) {
                mostOverlapsLengthCounter = secondValue[0];
                secondWordIndex = Number(secondKey);
                firstWordIndex = Number(firstKey);
                word = secondValue[1];
            }
        }
    }

    return {
        firstWordIndex,
        secondWordIndex,
        word,
        mostOverlapsLength: mostOverlapsLengthCounter,
    };
}

const updateArray = (wordsWithMostOverlaps: IWordsWithMostOverlaps, words: string[]): string[] => {
    const initialFirstWord = words[wordsWithMostOverlaps.firstWordIndex];
    const initialSecondWord = words[wordsWithMostOverlaps.secondWordIndex];
    const newWord = wordsWithMostOverlaps.word;

    const updatedArray = words.filter((word) => word !== initialFirstWord && word !== initialSecondWord);

    updatedArray.push(newWord);

    return updatedArray;
}

export const getSuperstring = (words: string[]): string => {
    // removes duplicates in initial list
    const wordsWithNoDuplicates = [...new Set(words)];

    if (wordsWithNoDuplicates.length === 0) {
        return '';
    }

    if (wordsWithNoDuplicates.length === 1) {
        return wordsWithNoDuplicates[0];
    }

    // gets all overlaps
    const allOverlapsIndexes = getAllOverlapsIndexes(wordsWithNoDuplicates);

    // finds words which has most overlaps and concatenate them
    const wordsWithMostOverlaps = getWordsWithMostOverlaps(allOverlapsIndexes);

    // removes two old words and add the generated one
    const updatedArray = updateArray(wordsWithMostOverlaps, wordsWithNoDuplicates);

    if (updatedArray.length > 1) {
        return getSuperstring(updatedArray);
    }

    return updatedArray[0];
}
